extends Node


# Порт сервера
const DEFAULT_PORT = 44444

# Максимальное количество клиентов
const MAX_PLAYERS = 10

# Словарь с подключенными клиентами id: name
var online_players = {}

# Генератор случайных чисел
var rnd = RandomNumberGenerator.new()
# Количество собрааной руды, кг
var m = {}
var c = {}
# Стоимость собранной руды
var i = {}

# Переменные вынес из функции _ready в глобальную область видимости, чтобы их можно было использовать в тестах
# Время затраченное на вылет корабля со станции
var delay1
# Время затраченное на полёт к астероидам
var delay2
# Время затраченное на дробление астероида
var delay3
# Время затраченное на сбор руды
var delay4
# Время затраченное на возвращение корабля к станции
var delay5
# Время затраченное на стыковку
var delay6
# Время затраченное на выгрузку руды
var delay7


# Массив виртуальных автономных игроков (для начала в виде NPC)
var players = { 
				'player1': 'Адам',
				'player2': 'Ждун',
				'player3': 'Хуан',
				'player4': 'Жанна',
				'player5': 'Люция',
				'player6': 'Ева'
			  }


func create_player_task(player_id):
	m[player_id] = rnd.randi_range(1005, 1505)
	c[player_id] = rnd.randi_range(900, 1100)
	i[player_id] = m[player_id] * c[player_id] / 1000
	delay1 = rnd.randi_range(3, 6)
	deferred_1(delay1, player_id)

	delay2 = rnd.randi_range(21, 27)+delay1
	deferred_2(delay2, player_id)

	delay3 = rnd.randi_range(9, 15)+delay2
	deferred_3(delay3, player_id)

	delay4 = rnd.randi_range(6, 9)+delay3
	deferred_4(delay4, player_id)
	
	delay5 = rnd.randi_range(21, 27)+delay4
	deferred_5(delay5, player_id)
	
	delay6 = rnd.randi_range(9, 15)+delay5
	deferred_6(delay6, player_id)
	
	delay7 = rnd.randi_range(6, 9)+delay6
	deferred_7(delay7, player_id)


func deferred_1(delay_time, player_id): # изменил название параметра с delay - номер п/п на одно значение, чтобы не было конфликта с глобальными переменными, которые имеют такие же имена
	yield(get_tree().create_timer(delay1), "timeout")
	print("Корабль тов. %s вылетел со станции! (Времязатраты: %s ч.)" % [players[player_id], delay_time])
	rpc("messaging", "Корабль тов. %s вылетел со станции! (Времязатраты: %s ч.)" % [players[player_id], delay_time])


func deferred_2(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Корабль тов. %s подлетел к астероидам! (Времязатраты: %s ч.)" % [players[player_id], delay_time])
	rpc("messaging", "Корабль тов. %s подлетел к астероидам! (Времязатраты: %s ч.)" % [players[player_id], delay_time])


func deferred_3(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Тов. %s закончил дробить астероид! (Времязатраты: %s ч.)" % [players[player_id], delay_time])
	rpc("messaging", "Тов. %s закончил дробить астероид! (Времязатраты: %s ч.)" % [players[player_id], delay_time])

	
func deferred_4(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Корабль тов. %s собрал минералы, добыто: %s кг. (Времязатраты: %s ч.)" % [players[player_id], m[player_id], delay_time])
	rpc("messaging", "Корабль тов. %s собрал минералы, добыто: %s кг. (Времязатраты: %s ч.)" % [players[player_id], m[player_id], delay_time])

	
func deferred_5(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Корабль тов. %s подлетел к станции! (Времязатраты: %s ч.)" % [players[player_id], delay_time])
	rpc("messaging", "Корабль тов. %s подлетел к станции! (Времязатраты: %s ч.)" % [players[player_id], delay_time])

	
func deferred_6(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Корабль тов. %s залетел на станцию! (Времязатраты: %s ч.)" % [players[player_id], delay_time])
	rpc("messaging", "Корабль тов. %s залетел на станцию! (Времязатраты: %s ч.)" % [players[player_id], delay_time])

	
func deferred_7(delay_time, player_id):
	yield(get_tree().create_timer(delay_time), "timeout")
	print("Тов. %s продал минералы за %s Инмо. (Времязатраты: %s ч.)" % [players[player_id], i[player_id], delay_time])
	rpc("messaging", "Тов. %s продал минералы за %s Инмо. (Времязатраты: %s ч.)" % [players[player_id], i[player_id], delay_time])
	# Цикл добычи завершен, освобождаем значения для повторного использования
	m.erase(player_id)
	c.erase(player_id)
	i.erase(player_id)


func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	
	create_server()
	
	rnd.randomize()


func create_server():
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(host)


# Callback from SceneTree, called when client connects
func _player_connected(_id):
	print("Пользователь %s подключился" % _id)


# Callback from SceneTree, called when client disconnects
func _player_disconnected(id):
	rpc("unregister_player", id)
	print("Пользователь %s отключился " % id)


# Player management functions
remote func register_player(new_player_name):
	# We get id this way instead of as parameter, to prevent users from pretending to be other users
	var caller_id = get_tree().get_rpc_sender_id()

	# Add him to our list
	online_players[caller_id] = new_player_name
	
	# Add everyone to new player:
	for p_id in online_players:
		rpc_id(caller_id, "register_player", p_id, online_players[p_id]) # Send each player to new dude
	
	rpc("register_player", caller_id, online_players[caller_id]) # Send new dude to all players
	# NOTE: this means new player's register gets called twice, but fine as same info sent both times
	
	print("Пользователь %s зарегистрирован как %s" % [caller_id, new_player_name])


puppetsync func unregister_player(id):
	print("Пользователь %s (%s) вышел" % [id, online_players[id]])

	online_players.erase(id)


# Цикл добычи ресурсов по массиву виртуальных игроков
func _process(delta):
	for key in players:
		if m.has(key):
			continue
		create_player_task(key)
		


# TODO: реализовать вывод информации подключившимся клиентам
